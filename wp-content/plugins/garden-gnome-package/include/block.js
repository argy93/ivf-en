/**
 * GGPKG Block
 *
 * To add GGPKG Content in Gutenberg Editor
 *
 */

( function(blocks, editor, components, i18n, element) {
	var el = element.createElement;
	var BlockControls = wp.editor.BlockControls;
	var InspectorControls = wp.editor.InspectorControls;
	var CheckboxControl = wp.components.CheckboxControl;
	var TextControl = wp.components.TextControl;
	var MediaUpload = editor.MediaUpload;
	var MediaPlaceholder = editor.MediaPlaceholder;
	var __ = i18n.__;

	const ggpkgIcon = el('svg', {width: 100, height: 100, viewBox: '0 0 447.6 469.2'},
		el('g', {}, 
			el('path', {fill: '#7F1818', d: 'M123.7,291.6c-14.6-15.2-32.7-65.3-8.8-111s41.7-56.6,46.3-63c4.6-6.4,25.2-79.1,60-103.8s64.5-12.2,84,5.3c19.5,17.4,33.1,48.7,18.8,61.6c-14.4,12.9-49.2,2.1-49.2,2.1l14,44.9c0,0,7.6,5.6,19.3,21.6c18.6,25.4,16.7,76.1,16.1,83.3c-6.5,71.9-20.6,71.2-63.4,70.5c-9.1-0.1-19.5-0.3-31.3,0.2C197.3,304.6,123.7,291.6,123.7,291.6'}),
			el('path', {fill: '#B32028', d: 'M199.5,104.5c0,0-40.6,21.1-55.7,43.5s-31.1,53.6-24.8,78.7c6.3,25.1,12.9,38,18.2,36c5.2-2,9-30.9,31.4-63.9s59.2-56.3,59.2-56.3s-25.7,5.4-41.7,17.2c-15.9,11.8-46.8,51.5-46.8,51.5s13.9-40.8,46.8-62.2s65.3-24.1,65.3-24.1s-16.2-5.9-31.6-4.3c-15.4,1.6-52.9,12.7-52.9,12.7s19-18,49.7-19.5c30.8-1.5,50,2.9,50,2.9s-0.7-33.1-4.2-42.7s-9.6-16.1-9.7-24.5s1.5-11.6,1.5-11.6s0.5,26.8,28.7,36.3c28.2,9.6,42,2.5,39.2-20.7s-25-38-39.6-45.4c-14.6-7.5-39.8-8.6-62.4,11.9s-47.4,90-47.4,90L199.5,104.5'}),
			el('path', {fill: '#B32028', d: 'M276.8,137c-41.7,7.1-69.2,27.2-87.7,43.9s-45.4,60.5-45.4,60.5s66.4,3.2,152.5-22.8c30.2-9.1,22.9-17.5,22.9-17.5S312.8,130.9,276.8,137'}),
			el('polyline', {fill: '#BD9563', points: '39.2,469.2 324.2,469.2 324.2,187.2 39.2,187.2 39.2,469.2 	'}),
			el('polyline', {fill: '#BD9563', points: '324.2,469.2 405.2,469.2 405.2,187.2 324.2,187.2 324.2,469.2 	'}),
			el('polyline', {fill: '#E7BE54', points: '353.2,469.2 376.2,469.2 376.2,269.3 353.2,269.3 353.2,469.2 	'}),
			el('g', {}, 
				el('polyline', {fill: '#997950', points: '405.2,187.2 324.2,187.2 324.2,469.2 353.2,469.2 353.2,269.3 376.2,269.3 376.2,469.2 405.2,469.2 405.2,187.2 		'}),
				el('polyline', {fill: '#BC9A44', points: '376.2,269.3 353.2,269.3 353.2,469.2 376.2,469.2 376.2,269.3 		'})
			),
			el('g', {},
				el('path', {fill: '#E8E8E8', d:'M324.2,187.2h-2.4l0,0H324.2L324.2,187.2'}),
				el('path', {fill: '#741616', d:'M321.8,187.2H317l0,0H321.8L321.8,187.2'}),
				el('polyline', {fill: '#E8E8E8', points:'111.7,187.2 39.1,187.2 39.2,187.2 39.2,187.2 111.7,187.2 111.7,187.2 		'}),
				el('path', {fill: '#741616', d:'M123.1,187.2h-11.4l0,0H123.1L123.1,187.2 M159.2,187.2h-8.1l0,0H159.2L159.2,187.2 M182.9,187.2h-5.5l0,0H182.9L182.9,187.2'}),
				el('path', {fill: '#A31D24', d:'M151.1,187.2h-28l0,0H151.1L151.1,187.2 M177.4,187.2h-18.2l0,0H177.4L177.4,187.2'}),
				el('polyline', {fill: '#A31D24', points:'317,187.2 182.9,187.2 182.9,187.2 317,187.2 317,187.2 		'}),
				el('polyline', {fill: '#AC885A', points:'324.2,187.2 321.8,187.2 317,187.2 182.9,187.2 177.4,187.2 159.2,187.2 151.1,187.2 123.1,187.2 111.7,187.2 39.2,187.2 39.2,187.2 109.1,383.3 160.8,383.3 160.8,305.3 202.5,305.3 202.5,383.3 324.2,383.3 324.2,187.2 		'}),
			),
			el('polygon', {fill: '#E0B37C', points: '39.2,187.2 0,305.3 286.4,305.3 324.2,187.2 	'}),
			el('polygon', {fill: '#BD9563', points: '324.2,187.2 361.8,305.3 447.6,305.3 410,187.2 	'}),
			el('polygon', {fill: '#E7BE54', points: '167.6,426.9 174.6,419.9 181.7,426.9 188.8,419.9 195.8,426.9 202.5,420.2 202.5,305.3 160.8,305.3 160.8,420.1 	'})
		),
		el('polyline', {fill: '#D2AD4C', points: '202.5,305.3 160.8,305.3 160.8,383.3 202.5,383.3 202.5,305.3 '})
	);

	blocks.registerBlockType( 'ggpkg/ggpkg-block', {
		title: __( 'GGPKG', 'ggpkg' ),
		description: __('A custom block for displaying a Garden Gnome Package.', 'ggpkg'),
		icon: ggpkgIcon,
		category: 'widgets',
		attributes: {
			attachmentID: {
				type: 'number'
			},
			imageUrl: {
				type: 'string'
			},
			startPreview: {
				type: 'boolean',
			},
			width: {
				type: 'string',
			},
			height: {
				type: 'string',
			}
		},

		edit: function(props) {
			var attributes = props.attributes;
			var height = attributes.height;
			var width = attributes.width;
			var startPreview = attributes.startPreview;

			// default values
			if (width === undefined) {
				props.setAttributes({width: '100%', height: '800px', startPreview: false })
			}

			var onSelectPackage = function (media) {
				return props.setAttributes({
					imageUrl: media.url,
					attachmentID: media.id,
				});
			};

			var startPreviewChanged = function(value) {
				return props.setAttributes({
					startPreview: value
				});
			};

			var widthChanged = function(value) {
				return props.setAttributes({
					width: value
				});
			};

			var heightChanged = function(value) {
				return props.setAttributes({
					height: value
				});
			};

			return [
				el(InspectorControls, {},
					el('div', {},
						el('br', {}),
						el(CheckboxControl, {
							heading: '',
							label: 'Start Preview',
							help: 'If the player should start with a preview image and a play button',
							checked: startPreview,
							onChange: startPreviewChanged
						}),
						el('br', {}),
						el(TextControl, {
							label: 'Width',
							onChange: widthChanged,
							value: width
						}),
						el(TextControl, {
							label: 'Height',
							onChange: heightChanged,
							value: height
						})
					)
				),
				el(BlockControls, { key: 'controls' },
					el('div', { className: 'components-toolbar' },
						el(MediaUpload, {
							onSelect: onSelectPackage,
							type: 'image/ggsw-package',
							render: function (obj) {
								return el(components.Button, {
									className: 'components-icon-button components-toolbar__control',
									onClick: obj.open
								},
								// Add Dashicon for media upload button.
								el('svg', { className: 'dashicon dashicons-edit', width: '20', height: '20' },
									el('path', { d: 'M2.25 1h15.5c.69 0 1.25.56 1.25 1.25v15.5c0 .69-.56 1.25-1.25 1.25H2.25C1.56 19 1 18.44 1 17.75V2.25C1 1.56 1.56 1 2.25 1zM17 17V3H3v14h14zM10 6c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm3 5s0-6 3-6v10c0 .55-.45 1-1 1H5c-.55 0-1-.45-1-1V8c2 0 3 4 3 4s1-3 3-3 3 2 3 2z' })
								))
							}
						})
					)
				),
				el('div', { className: props.className, padding: '50px' },
					el(MediaUpload, {
						onSelect: onSelectPackage,
						type: 'image/ggsw-package',
						value: attributes.attachmentID,
						render: function (obj) {
							return el(components.Button, {
							className: attributes.attachmentID ? 'image-button' : 'button button-large',
							style: {backgroundColor: !attributes.attachmentID ? '#ABC744' : 'white'},
							onClick: obj.open
							},
							!attributes.attachmentID ? __('Select Package','ggpkg') : el('img', { src: attributes.imageUrl })
							)
						}
					})
				)
			]
		},
		save: function(props) {
			return null;
		}
	})
})(
	window.wp.blocks,
	window.wp.editor,	
	window.wp.components,
	window.wp.i18n,
	window.wp.element
);
