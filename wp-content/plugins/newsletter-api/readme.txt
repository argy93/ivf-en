=== API Extension for Newsletter ===

== Changelog ==

= 2.0.2 =

* Added subscribers list
* Added newsletters list

= 2.0.1 =

* Fix class require for old versions

= 2.0.0 =

* New JSON API for Newsletter

= 1.0.0 =

* First Release

