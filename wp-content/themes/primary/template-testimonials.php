<?php /* Template Name: Testimonials page */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

Timber::render( array( 'template-testimonials.twig' ), $context );
