<?php /* Template Name: Home page */

$context = Timber::context();

$context['complex_slider'] = get_field( 'complex_slider' );
$context['why_us']         = get_field( 'why_us' );
$context['services']       = get_field( 'services' );
$context['statistic_info'] = get_field( 'statistic_info' );
$context['treatments']     = get_field( 'treatments' );
$context['news']           = new WP_Query( array(
  'orderby'          => 'date',
  'order'            => 'DESC',
  'showposts'        => 3,
  'post_status'      => 'publish',
  'cat'              => 9,
  'suppress_filters' => 0
) );
$context['testimonials']   = new WP_Query( array(
  'orderby'          => 'date',
  'order'            => 'DESC',
  'showposts'        => 7,
  'post_type'        => 'wpcr3_review',
  'post_status'      => 'publish',
  'suppress_filters' => 0
) );

foreach ( $context['testimonials']->posts as $index => $post ) {
  $post->meta         = get_post_meta( $post->ID );
  $post->post_content = wpautop( $post->post_content );
}

Timber::render( array( 'template-home.twig', 'page.twig' ), $context );
