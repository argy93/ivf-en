<?php /* Template Name: Thank page */

$context = Timber::context();

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
Timber::render( array( 'template-thank.twig' ), $context );
