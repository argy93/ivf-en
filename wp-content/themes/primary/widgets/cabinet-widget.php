<?php


class cabinet_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
			'cabinet_widget',
			// Widget name will appear in UI
			__( 'Cabinet Link', 'primary' ),
			// Widget description
			array( 'description' => __( 'cabinet link', 'primary' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		if ( isset( $instance['link'] ) ) {
			$link = esc_html( $instance['link'] );
		} else {
			$link = '';
		}

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$context         = Timber::context();
		$context['link'] = $link;
		$context['text'] = apply_filters('wpml_translate_single_string', 'Cabinet', 'primary', 'Cabinet Widget' );

		Timber::render( array( 'templates/widgets/cabinet-widget-front.twig' ), $context );
		// This is where you run the code and display the output
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
		if ( isset( $instance['link'] ) ) {
			$link = esc_html( $instance['link'] );
		} else {
			$link = '';
		}

		$context = Timber::context();

		$context['link'] = [
			'id'   => $this->get_field_id( 'link' ),
			'name' => $this->get_field_name( 'link' ),
			'link' => $link,
			'e'    => __( 'Link:', 'primary' )
		];

		Timber::render( array( 'templates/widgets/cabinet-widget-back.twig' ), $context );
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		if ( ! empty( $new_instance['link'] ) ) {
			$instance['link'] = $new_instance['link'];
		} else {
			$instance['link'] = '';
		}

		return $instance;
	}
}

register_widget( 'cabinet_widget' );
