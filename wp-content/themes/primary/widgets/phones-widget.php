<?php


class phones_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
			'phones_widget',
			// Widget name will appear in UI
			__( 'Phones List', 'primary' ),
			// Widget description
			array( 'description' => __( 'Dropdown phones list, on your site', 'primary' ), )
		);
	}

	// Creating widget front-end

	public function widget( $args, $instance ) {
		if ( isset( $instance['phones'] ) ) {
			$phones = json_decode($instance['phones']);
		} else {
			$phones = [''];
		}

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		$context          = Timber::context();
		$context['phones'] = [];

		foreach ($phones as $phone) {
			$context['phones'][] = [
				'phone' => preg_replace('/\D/', '', $phone),
				'label' => $phone
			];
		}

		Timber::render( array( 'templates/widgets/phones-widget-front.twig' ), $context );
		// This is where you run the code and display the output
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {

		if ( isset( $instance['phones'] ) ) {
			$phones = json_decode($instance['phones']);
		} else {
			$phones = [''];
		}

		$context          = Timber::context();

		$context['phones'] = [
			'id'    => $this->get_field_id( 'phones' ),
			'name'  => $this->get_field_name( 'phones' ),
			'values' => $phones,
			'e' => __( 'Phones:', 'primary' )
		];

		Timber::render( array( 'templates/widgets/phones-widget-back.twig' ), $context );
	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance          = array();

		if (!empty($new_instance['phones'])) {
			$instance['phones'] = json_encode($new_instance['phones']);
		} else {
			$instance['phones'] = json_encode(['']);
		}

		return $instance;
	}
}

register_widget( 'phones_widget' );