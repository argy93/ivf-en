import $ from 'jquery';
import 'slick-carousel';

export default () => {
  $('.js-testimonials__slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 1500,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '.l-testimonials .c-arrows__prev',
    nextArrow: '.l-testimonials .c-arrows__next',
    touchMove: false,
    pauseOnFocus: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1,
          arrow: false,
          dots: true,
          customPaging: (slider, i) => `<span class="c-dots-block__dot" data-thumb="${i}" ></span>`,
          appendDots: $('.l-testimonials .c-dots-block'),
        },
      },
    ],
  });
};
