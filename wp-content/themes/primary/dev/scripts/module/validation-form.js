import $ from 'jquery';
import 'jquery-validation';

export default () => {
  const jsName = $('#formerrors .js-name').text();
  const jsEmail = $('#formerrors .js-email').text();
  const jsDate = $('#formerrors .js-date').text();
  const jsTel = $('#formerrors .js-tel').text();
  const jsMessage = $('#formerrors .js-message').text();
  const jsCity = $('#formerrors .js-city').text();
  const jsCountry = $('#formerrors .js-country').text();


  $.validator.addMethod("phoneno", function(phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length > 9 &&
      phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
  }, "<br />Please specify a valid phone number");

  $.validator.addMethod("email", function(email_valid, element) {
    email_valid = email_valid.replace(/\s+/g, "");
    return this.optional(element) || email_valid.match(/^([\w\d\-\.]+)@{1}(([\w\d\-]{1,67})|([\w\d\-]+\.[\w\d\-]{1,67}))\.(([a-zA-Z\d]{2,4})(\.[a-zA-Z\d]{2})?)$/i);
  }, "Please specify a valid email");

  $('.c-contact-form').validate({
    rules: {
      name: 'required',
      email: {
        required: true,
        email: true,
      },

      date: {
        required: true,
        date: true,
      },

      tel: {
        required: true,
        phoneno: true,
      },

      country: {
        required: true,
      },
    },
    messages: {
      name: jsName,
      email: {
        required: jsEmail,
        email: 'Please specify a valid email',
      },
      date: jsDate,
      tel: {
        required: jsTel,
        phoneno: 'Please specify a valid phone number'
      },
      message: jsMessage,
      city: jsCity,
      agree: '',
      country: jsCountry,
    },
  });

  $('.c-form').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
    },
    messages: {
      email: {
        required: 'E-mail is required',
        email: 'Please specify a valid email',
      },
    },
  });
};
