import $ from 'jquery';
import 'jquery-validation';

export default () => {
  let counter = 0;
  const filesNameArr = [];
  const $form = $('.js-appointment-form');
  const $skypeCheckbox = $form.find('.js-skype-checkbox');

  $form.submit((event) => {
    event.preventDefault();

    if (!formValid($form)) {
      return;
    }

    const formData = getFormData($form);

    formDataRequest($form, formData);

    $form.addClass('is-sending');

    emptyFormHandler($form);
  });

  $skypeCheckbox.click(() => skypeStateHandler($form, $skypeCheckbox));

  $form.on('click', '.js-upload', (event) => {
    event.preventDefault();


    const notChanged = $(event.target).siblings('input:not(.is-changed)');

    if (notChanged.length > 0) {
      notChanged.first().click();
    } else {
      addFilesHandler($form, counter, filesNameArr);
    }

    // eslint-disable-next-line no-plusplus
    counter++;
  });

  $form.on('click', '.js-delete', (event) => {
    const name = $(event.target).siblings().text();
    const index = $.inArray(name, filesNameArr);

    $('.js-upload').siblings('input').eq(index).remove();

    filesNameArr.splice(index, 1);

    showFilesHandler($form, filesNameArr);
  });
};

const skypeStateHandler = ($form, $skypeCheckbox) => {
  const skypeInput = $form.find('.js-skype-field');
  if ($skypeCheckbox.attr('checked') === 'checked') {
    skypeInput.prop('disabled', false);
  } else {
    skypeInput.prop('disabled', true);
  }
};

const showFilesHandler = ($form, filesNameArr) => {
  const filesContainer = $form.find('.c-contact-form__files');
  const html = filesNameArr.map((item) => {
    const template = `<div class="c-contact-form__files-item">
                        <span>${item}</span>
                        <svg width="10px" height="10px" class="c-contact-form__icon-delete js-delete">
                          <use xlink:href="#close">
                        </svg>
                      </div>`;

    return template;
  }).join('');

  filesContainer.empty();
  filesContainer.append(html);
};

const formValid = $form => $form.valid();

const getFormData = ($form) => {
  const formData = new FormData();

  formData.append('name', $form.find('input[name="name"]').val());
  formData.append('phone', $form.find('input[name="phone"]').val());
  formData.append('email', $form.find('input[name="email"]').val());
  formData.append('date', $form.find('input[name="date"]').val());
  formData.append('skype', $form.find('input[name="skype"]').val());
  formData.append('city', $form.find('input[name="city"]').val());
  formData.append('message', $form.find('textarea[name="message"]').val());
  formData.append('country', $form.find('select[name="country"]').val());
  // eslint-disable-next-line no-shadow
  $form.find('input[name="download[]"]').each((counter, item) => {
    const file = $(item).prop('files')[0];

    if (file && fileTypeHandler(item) && fileSizeHandler(item)) {
      formData.append(`file_${counter}`, $(item).prop('files')[0]);
    }
  });

  formData.append('file_counter', $form.find('input[name="download[]"]').length - 1);

  return formData;
};

const fileTypeHandler = (fileInput) => {
  const fileType = $(fileInput).prop('files')[0].type;
  const mimeTypes = [
    'image/bmp',
    'image/x-windows-bmp',
    'image/gif',
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/svg+xml',
    'image/webp',
    'application/pdf',
    'application/mspowerpoint',
    'application/vnd.ms-powerpoint',
    'application/powerpoint',
    'application/x-mspowerpoint',
    'text/plain',
    'text/csv',
    'application/excel',
    'application/x-excel',
    'application/x-msexcel',
    'application/vnd.ms-excel',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  ];

  return $.inArray(fileType, mimeTypes) !== -1;
};

const fileSizeHandler = (fileInput) => {
  const fileSize = $(fileInput).prop('files')[0].size;

  return fileSize < 1024 * 100;
};

const formDataRequest = ($form, formData) => {
  $.ajax({
    url: '/wp-json/primary/v1/forms/rest-visit-doctor',
    data: formData,
    processData: false,
    contentType: false,
    type: 'POST',
    success() {
      const formUrl = $form.attr('data-url');

      // eslint-disable-next-line no-restricted-globals
      $(location).attr('href', formUrl);
    },
  });
};

const emptyFormHandler = ($form) => {
  $form.find('input').val('');
  $form.find('textarea').val('');
  $form.find('.is-changed').remove();
  $form.find('.c-contact-form__files').empty();
};

const addFilesHandler = ($form, counter, filesNameArr) => {
  $form
    .find('.js-upload')
    .parent()
    .append(`<input type="file" name="download[]" id="download-${counter}">`);

  $(`#download-${counter}`)
    .on('change', (event) => {
      const $target = $(event.target);
      const filePath = $target.val()
        .split('/')
        .pop()
        .split('\\')
        .pop();

      const isEmpty = filePath !== '';
      const isType = fileTypeHandler($target);
      const isSize = fileSizeHandler($target);

      $('.c-contact-form__errorwrap').html('');

      if (isEmpty && isType && isSize) {
        filesNameArr.push(filePath);
        $target.addClass('is-changed');
      } else if (!isEmpty) {
        $('.c-contact-form__errorwrap').html($('#formerrors .js-emptyfile').html());
      } else if (!isType) {
        $('.c-contact-form__errorwrap').html($('#formerrors .js-typefile').html());
      } else if (!isSize) {
        $('.c-contact-form__errorwrap').html($('#formerrors .js-sizefile').html());
      }

      showFilesHandler($form, filesNameArr);
    })
    .trigger('click');
};
