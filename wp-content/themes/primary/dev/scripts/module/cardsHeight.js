import $ from 'jquery';

export default () => {
  let maxHeight = 0;

  // Нужно вынести скрипт в отдельную вункцию, и запускать сразу и 2-ой раз при загрузке картинок (событие onload)

  $('.c-treat-card').each((index, el) => {
    const elHeight = $(el).outerHeight();
    maxHeight = (elHeight > maxHeight) ? elHeight : maxHeight;
  });
  $('.c-treat-card').css('height', maxHeight);
};
