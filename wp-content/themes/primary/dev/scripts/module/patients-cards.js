import $ from 'jquery';

export default () => {
  let maxHeight = 0;

  $('.c-testimonials-card').each((index, el) => {
    const elHeight = $(el).height();
    maxHeight = (elHeight > maxHeight) ? elHeight : maxHeight;
  });

  $('.c-testimonials-card').height(maxHeight);
};
