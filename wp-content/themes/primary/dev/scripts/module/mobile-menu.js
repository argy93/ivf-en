import $ from 'jquery';

export default () => {
  if ($(window).innerWidth() <= 1200) {
    const $modalContainer = $('.l-modal-container');

    $('.c-menu__hamburger').click(() => {
      $modalContainer.addClass('is-active');
      $modalContainer.children('.c-modal').addClass('is-active');
    });

    $('.l-modal-container__close').click(() => {
      $modalContainer.removeClass('is-active');
      $modalContainer.children('.c-modal').removeClass('is-active');
    });

    $('.c-main-nav__item a').click((event) => {
      showList(event.currentTarget, event);
    });
  }
};

const showList = (el) => {
  const submenu = $(el).siblings();

  $(submenu)
    .closest('.is-show')
    .find('.is-show')
    .removeClass('is-show')
    .slideUp(350)
    .prev()
    .removeClass('is-active-item')
    .find('.c-main-nav__caret')
    .css('transform', 'rotate(0deg)');

  if (!submenu.hasClass('is-show')) {
    $(submenu).addClass('is-show')
      .slideDown(350)
      .prev()
      .addClass('is-active-item')
      .find('.c-main-nav__caret')
      .css('transform', 'rotate(180deg)');
  } else {
    $(submenu).removeClass('is-show')
      .slideUp(350)
      .prev()
      .removeClass('is-active-item')
      .find('.c-main-nav__caret')
      .css('transform', 'rotate(0deg)');
  }
};
