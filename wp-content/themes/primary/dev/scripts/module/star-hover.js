import $ from 'jquery';

export default () => {
  const firstTarget = $('.l-main__slide');
  const firstStarEmpty = $('.l-main__svg .is-star-empty');
  const firstStarFull = $('.l-main__svg .is-star-full');

  const secondTarget = $('.js-star-hover');
  const secondStarEmpty = $('.l-write-to-us__img .is-star-empty');
  const secondStarFull = $('.l-write-to-us__img .is-star-full');

  const thirdTarget = $('.l-write-to-us__img');

  const fourTarget = $('.l-welcome__text');
  const fourStarEmpty = $('.l-welcome__svg .is-star-empty');
  const fourStarFull = $('.l-welcome__svg .is-star-full');

  starHoverHandler(firstTarget, firstStarEmpty, firstStarFull);
  starHoverHandler(secondTarget, secondStarEmpty, secondStarFull);
  starHoverHandler(thirdTarget, secondStarEmpty, secondStarFull);
  starHoverHandler(fourTarget, fourStarEmpty, fourStarFull);
};

const starHoverHandler = (target, emptyStar, fullStar) => {
  target.hover(
    () => {
      emptyStar.fadeOut(300);
      fullStar.fadeIn(300);
    },
    () => {
      emptyStar.fadeIn(300);
      fullStar.fadeOut(300);
    },
  );
};
