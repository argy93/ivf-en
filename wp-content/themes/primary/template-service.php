<?php
/* Template Name: Service
 * Template Post Type: post, product
 */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

Timber::render( array( 'template-service.twig' ), $context );
