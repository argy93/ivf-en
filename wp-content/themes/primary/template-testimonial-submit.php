<?php
/* Template Name: Testimonial submit page
 * Template Post Type: post, page, product
 */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

Timber::render( array( 'template-testimonial-submit.twig' ), $context );
