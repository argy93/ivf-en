module.exports = (env, options) => {

  const production = options.mode === 'production';
  const path = require('path');
  const ExtractTextPlugin = require('extract-text-webpack-plugin');
  const ImageminPlugin = require('imagemin-webpack-plugin').default;
  const CopyWebpackPlugin = require('copy-webpack-plugin');
  const CleanWebpackPlugin = require('clean-webpack-plugin');
  const MD5FilesRename = require('./dev/node/md5-webpack-provider');
  const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
  const browser_sync_config = require('./browsersync.config');
  const SassLintPlugin = require('sass-lint-webpack');
  const imageminMozjpeg = require('imagemin-mozjpeg');
  const autoprefixer = require('autoprefixer');
  const md5_files_rename = new MD5FilesRename({
    'production': production,
    'path_to_hash': './version.md5',
    'exclude': ['block-graph']
  });
  const plugins = [
    new SassLintPlugin(),
    new CleanWebpackPlugin('build', {}),
    md5_files_rename,
    new CopyWebpackPlugin([{
      from: './dev/images',
      to: 'static/images'
    }]),
    new CopyWebpackPlugin([{
      from: './dev/fonts',
      to: 'static/fonts'
    }]),
    new ExtractTextPlugin({
      filename: (getPath) => {
        const name = getPath('[name]');
        if (name === 'block-graph') return `blocks/${name}.css`;
        return `static/${name}.css`;
      }
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      proxy: browser_sync_config.host,
      port: browser_sync_config.port,
      ui: false
    })
  ];

  if (production) {

    plugins.push(new ImageminPlugin({
      cacheFolder: 'cache',
      pngquant: ({quality: '68'}),
      plugins: [imageminMozjpeg({quality: '68'})]
    }));
  }

  let conf = {
    entry: {
      'main': './dev/scripts/main.js',
      'editor': './dev/scripts/editor.js',
      'block-graph': './dev/scripts/block-graph.js'
      //'critical-home': './dev/scripts/critical-home.js'
    },
    output: {
      path: path.resolve(__dirname, './'),
      filename: (chunkData) => {
        const name = chunkData.chunk.name;

        // if (name === 'block-graph') return 'blocks/[name].js';

        return 'static/[name].js';
      },
      publicPath: '../'
    },
    watchOptions: {
      aggregateTimeout: 300, // <---------
      poll: 1000, // <---------
      ignored: '/node_modules/'
    },
    resolveLoader: {
      modules: ['node_modules', './node/loader']
    },
    resolve: {
      alias: {
        '~': path.resolve(__dirname, './node_modules')
      }
    },
    externals: {
      jquery: 'jQuery'
    },
    module: {
      rules: [

        {
          test: /[\\\/]node_modules[\\\/]modernizr[\\\/]modernizr\.js$/,
          loader: "imports?this=>window!exports?window.Modernizr"
        },
        {
          enforce: "pre",
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "eslint-loader"
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          query: {
            cacheDirectory: true
          },
          exclude: /(node_modules|bower_components)/,

        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader'
          })
        },
        {
          test: /\.scss/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              'css-loader',
              {
                loader: 'postcss-loader',
                options: {
                  plugins: [
                    autoprefixer()
                  ],
                }
              }, 'sass-loader']
          })
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: 'images/[name].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
          loader: 'file-loader',
          query: {
            limit: 10000,
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    },
    plugins: plugins,
    devtool: 'eval-sourcemap'
  };

  conf.devtool = production
    ? false   /*'sourcemap'*/
    : 'eval-sourcemap';

  return conf;
};
