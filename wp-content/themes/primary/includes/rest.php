<?php

add_filter( 'restsplain_config', function( $config ) {
  $config['codeTheme'] = 'Agate';
  return $config;
} );

add_action( 'rest_api_init', function () {
  register_rest_route( 'primary/v1', '/forms/rest-letter-subscribe', array(
    'methods'  => 'POST',
    'callback' => 'rest_letter_subscribe',
    'args'     => array(
      'email' => array(
        'required'          => true,
        'validate_callback' => function ( $param, $request, $key ) {
          return filter_var( $param, FILTER_VALIDATE_EMAIL );
        }
      ),
    )
  ) );

  register_rest_route( 'primary/v1', '/forms/rest-visit-doctor', array(
    'methods'  => 'POST',
    'callback' => 'rest_visit_subscribe',
    'args'     => array(
      'email' => array(
        'required'          => true,
        'validate_callback' => function ( $param, $request, $key ) {
         return filter_var( $param, FILTER_VALIDATE_EMAIL );
        }
      ),
    )
  ) );
} );

function rest_visit_subscribe(WP_REST_Request $data) {
  $form_data = $data->get_params();

  $to       = get_bloginfo('admin_email');
  $subject  = "Appointment Request";
  $subject  = "=?utf-8?B?".base64_encode($subject)."?=";
  $message  = "<html><body style='font-family:Arial,sans-serif;'>";
  $message .= "<h2 style='font-weight:bold;border-bottom:1px dotted #ccc;'>Cообщение с сайта</h2>\r\n";
  $message .= "<p><strong>От кого:</strong> ".$form_data['name']."</p>\r\n";
  $message .= "<p><strong>Сообщение:</strong> ".$form_data['message']."</p>\r\n";
  $message .= "<p><strong>Почта:</strong> ".$form_data['email']."</p>\r\n";
  $message .= "<p><strong>Телефон:</strong> ".$form_data['phone']."</p>\r\n";
  $message .= "<p><strong>Скайп:</strong> ".$form_data['skype']."</p>\r\n";
  $message .= "<p><strong>Дата:</strong> ".$form_data['date']."</p>\r\n";
  $message .= "<p><strong>Страна:</strong> ".$form_data['country']."</p>\r\n";
  $message .= "<p><strong>Город:</strong> ".$form_data['city']."</p>\r\n";
  $message .= "</body></html>";
  // $message = $form_data['message'];

  $headers = array(
    "charset=utf-8",
    "content-type: text/plain",
    "MIME-Version: 1.0\r\n",
    "From: " . $form_data['email'],
    "Reply-to: " . $form_data['email']
  );

  $attachment = move_file_to_uploads($_FILES);

  if ( wp_mail( $to, $subject, $message, $headers, $attachment ) ) {
    return 'Transmission successfull!';
  } else {
    return 'Request Error!!!';
  }
}

function move_file_to_uploads($filesArr) {
  $allow_size = 100000;
  $mimeTypes = array(
    'image/bmp',
    'image/x-windows-bmp',
    'image/gif',
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/svg+xml',
    'image/webp',
    'application/pdf',
    'application/mspowerpoint',
    'application/vnd.ms-powerpoint',
    'application/powerpoint',
    'application/x-mspowerpoint',
    'text/plain',
    'text/csv',
    'application/excel',
    'application/x-excel',
    'application/x-msexcel',
    'application/vnd.ms-excel',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  );

  if( $filesArr ) {

    $done_files = array();

    foreach( $filesArr as $file ){
      $file_name = $file['name'];

      if ( $file['size'] < $allow_size  && in_array($file['type'], $mimeTypes) ) {
        $done_files[] = $file['tmp_name'];
      }
    }

    return $done_files;
  }
}

function rest_letter_subscribe(WP_REST_Request $data) {
//  return TNP::subscribe(['email'=> $data['email']]);
}
